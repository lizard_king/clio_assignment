from marshmallow import Schema, fields

from nodes import Node, DeveloperNode, ManagerNode


class NodeSchema(Schema):
    class Meta:
        ordered = True

    model_class = Node

    id = fields.Integer(dump_only=True)
    name = fields.String(required=True)
    height = fields.Integer(dump_only=True)
    children = fields.List(fields.Nested("self", only=("name", "id", "height")))
    parent_id = fields.Integer(load_only=True)
    role = fields.Constant("CEO", dump_only=True)


class ManagerNodeSchema(NodeSchema):
    class Meta:
        ordered = True

    model_class = ManagerNode
    role = fields.Constant("manager", dump_only=True)
    department = fields.String(required=True)


class DeveloperNodeSchema(NodeSchema):
    class Meta:
        ordered = True

    model_class = DeveloperNode
    role = fields.Constant("developer", dump_only=True)
    language_best_at = fields.String(required=True)


class ChildSchema(Schema):
    id = fields.Integer(dump_only=True)
    name = fields.String(dump_only=True)
    height = fields.Integer(dump_only=True)
