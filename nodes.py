from dataclasses import dataclass
from typing import Union, List


@dataclass
class Node:
    id: int
    name: str
    parent: Union[object, None]
    height: int
    children: List[object]

    @property
    def is_leaf(self):
        return not self.children

    def __str__(self):
        return f"Node(id={self.id} name={self.name})"

    def __eq__(self, other):
        return self.id == other.id


@dataclass
class ManagerNode(Node):
    department: str

    def __str__(self):
        return f"ManagerNode(id={self.id} name={self.name})"


@dataclass
class DeveloperNode(Node):
    language_best_at: str

    def __str__(self):
        return f"DeveloperNode(id={self.id} name={self.name})"


CEO_NODE = Node(
    id=0,
    name="Pernille",
    parent=None,
    height=0,
    children=[],
)
