from flask import Flask, jsonify, request, Response
from flask_restful import Api, Resource

from nodes import CEO_NODE
from nodes import DeveloperNode, ManagerNode
from schemas import DeveloperNodeSchema, ManagerNodeSchema, ChildSchema
from tree import get_or_create_tree, cache_tree

app = Flask(__name__)
api = Api(app, catch_all_404s=True)

tree = get_or_create_tree()


class AddNodeResource(Resource):
    def post(self):
        payload = request.get_json(force=True)

        try:
            if "role" not in payload.keys():
                raise Exception("Specify the role of the node you are creating!")
            if payload["role"].lower() not in ("developer", "manager"):
                raise Exception("Unknown role!")

            role = payload.pop("role")

            if role == "developer":
                new_developer_data = DeveloperNodeSchema().load(data=payload)
                new_developer_node = tree.add_developer_node(**new_developer_data)
                return DeveloperNodeSchema().dump(new_developer_node)
            if role == "manager":
                parent_node = tree.get_node_by_id(payload["parent_id"])
                if isinstance(parent_node, DeveloperNode):
                    raise Exception("Developers should not manage managers!")

                new_manager_data = ManagerNodeSchema().load(data=payload)
                new_manager_node = tree.add_manager_node(**new_manager_data)
                return ManagerNodeSchema().dump(new_manager_node)
        except Exception as e:
            return jsonify({"error": str(e)})


class ChildrenResource(Resource):
    def get(self, id):
        try:
            node = tree.get_node_by_id(id)
            return ChildSchema(many=True).dump(node.children)
        except Exception as e:
            return jsonify({"error": str(e)})


class ChangeParentNodeResource(Resource):
    def patch(self, id):
        try:
            if id == CEO_NODE.id:
                raise Exception("Cannot change CEO's parent node!")

            payload = request.get_json(force=True)
            new_parent_id = payload.pop("new_parent_id", None)

            if id == new_parent_id:
                raise Exception("Cannot change parent to self!")

            if not new_parent_id:
                raise Exception("new_parent_id field is required")

            node, new_node_parent = tree.get_node_by_id(id), tree.get_node_by_id(new_parent_id)
            if isinstance(node, ManagerNode) and isinstance(new_node_parent, DeveloperNode):
                raise Exception("Developers should not manage managers!")

            tree.change_node_parent(node, new_node_parent)
            return Response(f"Parent for {node.id} changed to node with id {new_node_parent.id}")

        except Exception as e:
            return jsonify({"error": str(e)})


api.add_resource(AddNodeResource, "/nodes")
api.add_resource(ChildrenResource, "/nodes/children/<int:id>")
api.add_resource(ChangeParentNodeResource, '/nodes/change_parent/<int:id>')

if __name__ == '__main__':
    try:
        app.run(port=8080)
    finally:
        cache_tree(tree)
