import shelve
from pathlib import Path

from nodes import Node, ManagerNode, DeveloperNode, CEO_NODE


class Tree:
    def __init__(self, root=CEO_NODE, nodes=None):
        if not nodes:
            self.nodes = [root]
        else:
            self.nodes = [root, *nodes]

    @property
    def max_id(self):
        return max(node.id for node in self.nodes)

    def add_node(self, node: Node):
        self.nodes.append(node)

    def get_node_by_id(self, node_id: int) -> Node:
        try:
            return [node for node in self.nodes if node.id == node_id].pop()
        except IndexError:
            raise Exception(f"The node with id {node_id} does not exist")

    def add_manager_node(self, name: str, parent_id: int, department: str) -> ManagerNode:
        parent_node = self.get_node_by_id(parent_id)

        new_manager_node = ManagerNode(
            id=self.max_id + 1,
            name=name,
            parent=parent_node,
            height=parent_node.height + 1,
            department=department,
            children=[],
        )
        self.add_node(new_manager_node)
        parent_node.children.append(new_manager_node)
        return new_manager_node

    def add_developer_node(self, name: str, parent_id: int, language_best_at: str) -> DeveloperNode:
        parent_node = self.get_node_by_id(parent_id)

        new_developer_node = DeveloperNode(
            id=self.max_id + 1,
            name=name,
            parent=parent_node,
            height=parent_node.height + 1,
            language_best_at=language_best_at,
            children=[]
        )
        self.add_node(new_developer_node)
        parent_node.children.append(new_developer_node)
        return new_developer_node

    def change_node_parent(self, node: Node, new_parent: Node):
        node.parent.children.remove(node)
        node.parent = new_parent
        new_parent.children.append(node)
        node.height = new_parent.height + 1

        if not node.is_leaf:
            for child in node.children:
                self.change_node_parent(child, node)


def get_or_create_tree():
    """
    Returns tree that was previously cached or initializes new one
    :return: Tree instance
    """

    project_directory = Path(__file__).parent
    if "tree.db" in [file.name for file in project_directory.iterdir()]:
        cache = shelve.open(str(project_directory.joinpath("tree.db")))
        try:
            return cache["tree"]
        finally:
            cache.close()
    else:
        return Tree()


def cache_tree(tree: Tree):
    """
    Saves tree instance after the server shutdown
    :param tree: Tree instance
    """

    project_directory = Path(__file__).parent
    cache = shelve.open(str(project_directory.joinpath("tree.db")))
    try:
        cache["tree"] = tree
    finally:
        cache.close()
