# clio_assignment

**[This is the task I was given](taxonomy.md)**

### What was used for the implementation 
**Python 3.8** as a programming language  
**Flask-restful** as a backend framework  
**marshmallow** as a serialization lib

_I decided not to use a database for this task but to keep node  
records storage persistent I used Python's standard library module  
"shelve" which is used to cache Python objects into a .db file._


### How to run it

1) You will need Python 3.8 installed on your computer  
   To install it on Linux (apt-based distibutions) run the following commands:  
   ``sudo apt update ``  
   ``sudo apt install software-properties-common``  
   ``sudo add-apt-repository ppa:deadsnakes/ppa``  
   ``sudo apt install python3.8``  
   For mac run the following command:  
   ``brew install python@3.8``  
2) To create the virtual environment where 3rd-party libraries will be installed run this:  
   ``python3.8 -m venv .venv``
3) Activate virtual environment:  
   ``source ./.venv/bin/activate``
4) Install dependencies using the following command:  
   ``pip3 install -r requirements.txt``
5) Run the server:  
   ``python3.8 server.py``
6) If you want to shut down the server press Ctrl+C in the terminal you've launched the app from.   

### API  

#### Creating Node

Endpoint: **/nodes**  
Request example:    
``
curl --location --request POST 'http://localhost:8080/nodes' 
--header 'Content-Type: application/json'
--data-raw '{  
    "name": "Dave",  
    "role": "manager",  
    "parent_id": 0,
    "department": "Java"
}'
``  

Response example:
``
{
    "id": 2,
    "name": "Dave",
    "height": 1,
    "children": [],
    "role": "manager",
    "department": "Java"
}
``

#### Getting node's children (1 lvl)  
Endpoint: **/nodes/children/_NODE_ID_**    
Request example:    
``
curl --location --request GET 'http://localhost:8080/nodes/children/0'
``

Response example:
``
[
    {
        "name": "Dave",
        "height": 1,
        "id": 1
    },
    {
        "name": "Brian",
        "height": 1,
        "id": 2
    }
]
``
  
#### Changing node's parent  
Endpoint: **/nodes/change_parent/_NODE_ID_**  
Request Example:  
``
curl --location --request PATCH 'http://localhost:8080/nodes/change_parent/1' \
--header 'Content-Type: application/json' \
--data-raw '{
    "new_parent_id": 2
}'
``

Response example:  
``Parent for 1 changed to node with id 2``


### What I would improve

1) I would work more on the serialization so that the output of the endpoints would be more informative and clear  
2) I would test the app more and maybe would find more edge cases and cover them 
3) I would create more endpoints to implement the full CRUD package for nodes but to save time I implemented only those I was asked for  
